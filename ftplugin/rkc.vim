" Vim filetype plugin
" Language:     RKC
" Creator and maintainer of RKC:   Tristan Ninet <ninettristan@gmail.fr>
" Creator and maintainer of this fork of RKC:   mahaaaham <mahaaaham@protonmail.com>
" Last Change:  2021 Aug 28

"========================
" Miscellaneous 
"========================

if exists("b:did_ftplugin")
  finish
endif

setlocal comments=fb:-
setlocal formatoptions=tcq
" setlocal formatlistpat=^\s*-\s*

if exists('b:undo_ftplugin')
  let b:undo_ftplugin .= "|setl cms< com< fo< flp<"
else
  let b:undo_ftplugin = "setl cms< com< fo< flp<"
endif

"========================
" Folding 
"========================

function! RkcFold()
  let line = getline(v:lnum)

  " Regular headers
  let depth = match(line, '\(^#\+\)\@<=\( .*$\)\@=')
  if depth > 0
    return ">" . depth
  endif

  return "="
endfunction

if has("folding")
  setlocal foldexpr=RkcFold()
  setlocal foldmethod=expr
  let b:undo_ftplugin .= " foldexpr< foldmethod<"
endif

" vim:set sw=2:
syntax on
