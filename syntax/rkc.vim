" Vim syntax file
" Language:     RKC
" Creator and maintainer of RKC:   Tristan Ninet <ninettristan@gmail.fr>
" Creator and maintainer of this fork of RKC:   mahaaaham <mahaaaham@protonmail.com>
" Filenames:    *.rkc
" Last Change:  2021 Aug 28

if exists("b:current_syntax")
  finish
endif

" Definition of local colors, acconding to the Color Mode choose.
" The Default Color Mode is 'Light'.
if !exists("g:RkcColorMode") || (g:RkcColorMode=="Light")
  let s:RkcTermColorH1="1"
  let s:RkcTermColorH2="4"
  let s:RkcTermColorH3="5"
  let s:RkcTermColorH4="2"
  let s:RkcTermColorH5="3"
  let s:RkcTermColorH6="6"
  let s:RkcTermColorListMarker="33"
  let s:RkcTermColorWeb="21"
  let s:RkcTermColorQuote="241"
  " Gui
  let s:RkcGuiColorH1="DarkRed"
  let s:RkcGuiColorH2="DarkBlue"
  let s:RkcGuiColorH3="DarkMagenta"
  let s:RkcGuiColorH4="DarkGreen"
  let s:RkcGuiColorH5="DarkYellow"
  let s:RkcGuiColorH6="DarkCyan"
  let s:RkcGuiColorListMarker="DarkCyan"
  let s:RkcGuiColorWeb="Blue"
  let s:RkcGuiColorQuote="Grey"
elseif (g:RkcColorMode=="Dark")
  let s:RkcTermColorH1="173"
  let s:RkcTermColorH2="69"
  let s:RkcTermColorH3="133"
  let s:RkcTermColorH4="121"
  let s:RkcTermColorH5="229"
  let s:RkcTermColorH6="147"
  let s:RkcTermColorListMarker="LightGrey"
  let s:RkcTermColorWeb="45"
  let s:RkcTermColorQuote="253"
  " Gui
  let s:RkcGuiColorH1="LightRed"
  let s:RkcGuiColorH2="LightBlue"
  let s:RkcGuiColorH3="LightMagenta"
  let s:RkcGuiColorH4="LightGreen"
  let s:RkcGuiColorH5="LightYellow"
  let s:RkcGuiColorH6="LightCyan"
  let s:RkcGuiColorListMarker="DarkCyan"
  let s:RkcGuiColorWeb="Blue"
  let s:RkcGuiColorQuote="Grey"
else
  echo 'RkcColorMode can only be set to "Light" or "Dark"'
endif

" Definition of local delimiters (only usefull if g:RkcExtra==1)
let s:RkcBeginQuote="__q__"
let s:RkcEndQuote="__q__"


if !exists('main_syntax')
  let main_syntax = 'rkc'
endif

unlet! b:current_syntax

" Set the global variables to the value of the s: ones, unless if the user
" defines them in vimrc
if !exists("g:RkcTermColorH1")
  let g:RkcTermColorH1=s:RkcTermColorH1
endif
if !exists("g:RkcTermColorH2")
  let g:RkcTermColorH2=s:RkcTermColorH2
endif
if !exists("g:RkcTermColorH3")
  let g:RkcTermColorH3=s:RkcTermColorH3
endif
if !exists("g:RkcTermColorH4")
  let g:RkcTermColorH4=s:RkcTermColorH4
endif
if !exists("g:RkcTermColorH5")
  let g:RkcTermColorH5=s:RkcTermColorH5
endif
if !exists("g:RkcTermColorH6")
  let g:RkcTermColorH6=s:RkcTermColorH6
endif
if !exists("g:RkcTermColorListMarker")
  let g:RkcTermColorListMarker=s:RkcTermColorListMarker
endif
if !exists("g:RkcTermColorWeb")
  let g:RkcTermColorWeb=s:RkcTermColorWeb
endif
if !exists("g:RkcTermColorQuote")
  let g:RkcTermColorQuote=s:RkcTermColorQuote
endif
" Gui
if !exists("g:RkcGuiColorH1")
  let g:RkcGuiColorH1=s:RkcGuiColorH1
endif
if !exists("g:RkcGuiColorH2")
  let g:RkcGuiColorH2=s:RkcGuiColorH2
endif
if !exists("g:RkcGuiColorH3")
  let g:RkcGuiColorH3=s:RkcGuiColorH3
endif
if !exists("g:RkcGuiColorH4")
  let g:RkcGuiColorH4=s:RkcGuiColorH4
endif
if !exists("g:RkcGuiColorH5")
  let g:RkcGuiColorH5=s:RkcGuiColorH5
endif
if !exists("g:RkcGuiColorH6")
  let g:RkcGuiColorH6=s:RkcGuiColorH6
endif
if !exists("g:RkcGuiColorListMarker")
  let g:RkcGuiColorListMarker=s:RkcGuiColorListMarker
endif
if !exists("g:RkcGuiColorWeb")
  let g:RkcGuiColorWeb=s:RkcGuiColorWeb
endif
if !exists("g:RkcGuiColorQuote")
  let g:RkcGuiColorQuote=s:RkcGuiColorQuote
endif
if !exists("g:RkcBeginQuote")
  let g:RkcBeginQuote=s:RkcBeginQuote
endif
if !exists("g:RkcEndQuote")
  let g:RkcEndQuote=s:RkcEndQuote
endif

" Makes parsing faster?
syntax sync minlines=10
syntax case ignore

" Backspace have to be doubled in the string, see "help string"
call rkc#fun#RkcHiRegion("rkcH1", "^#", "^\\s*$",  
                        \"bold", g:RkcTermColorH1, 
                        \"bold", g:RkcGuiColorH1)
call rkc#fun#RkcHiRegion("rkcH2", "^##", "^\\s*$", 
                        \"bold", g:RkcTermColorH2, 
                        \"bold", g:RkcGuiColorH2)
call rkc#fun#RkcHiRegion("rkcH3", "^###", "^\\s*$", 
                        \"bold", g:RkcTermColorH3, 
                        \"bold", g:RkcGuiColorH3)
call rkc#fun#RkcHiRegion("rkcH4", "^####", "^\\s*$", 
                        \"bold", g:RkcTermColorH4, 
                        \"bold", g:RkcGuiColorH4)
call rkc#fun#RkcHiRegion("rkcH5", "^#####", "^\\s*$", 
                        \"bold", g:RkcTermColorH5, 
                        \"bold", g:RkcGuiColorH5)
call rkc#fun#RkcHiRegion("rkcH6", "^######", "^\\s*$", 
                        \"bold", g:RkcTermColorH6, 
                        \"bold", g:RkcGuiColorH6)
call rkc#fun#RkcHi("rkcListMarker", "^\\s*-   ", 
                  \"italic", g:RkcTermColorListMarker, 
                  \"italic", g:RkcGuiColorListMarker)

" extra features
if exists("g:RkcExtra") && (g:RkcExtra==1)
  call rkc#fun#RkcHi("rkcWeb", "\\(https:\\/\\/\\|http:\\/\\/\\|\\s\\+www\\.\\|^www.\\)\\S\\+", 
                    \"italic", g:RkcTermColorWeb, 
                    \"italic", g:RkcGuiColorWeb)
  call rkc#fun#RkcHiRegionConceal("rkcQuote", g:RkcBeginQuote, g:RkcEndQuote,
                    \"italic", g:RkcTermColorQuote, 
                    \"italic", g:RkcGuiColorQuote)
endif


let b:current_syntax = "rkc"
if main_syntax ==# 'rkc'
  unlet main_syntax
endif

setlocal conceallevel=3

" What to do of it?
" vim:set sw=2:
