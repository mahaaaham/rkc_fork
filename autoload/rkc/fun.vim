" This functions will hightlight the text contained in the pattern delimited by begin_pattern and 
" end_pattern variables, with  termfg for terminal color and guifg for gui color. 
" In order to do this, it will create regions with names:
" the value of name_region
" the value of name_region concatenated with _begin
" the value of name_region concatenated with _end
function rkc#fun#RkcHiRegion(name_region, begin_pattern, end_pattern, term, termfg, gui, guifg)
  " If we want to use string variables in a command, we can create a string that contains the command with
  " the value of variable, then execute the string.
  
  let l:name_region_begin = a:name_region.'_begin'
  let l:name_region_end = a:name_region.'_end'
  
  let l:syntax_begin ='syntax match '.l:name_region_begin.' "'.a:begin_pattern.'"' 
  let l:syntax_end = 'syntax match '.l:name_region_end.' "'.a:end_pattern.'"'

  execute l:syntax_begin
  execute l:syntax_end

  let l:syntax_region = 'syntax region '.a:name_region.' start="'.a:begin_pattern.'" end="'.a:end_pattern.'"' 
  execute l:syntax_region
  let l:hightlight = 'highlight def '.a:name_region.' cterm='.a:term.' ctermfg='.a:termfg.' gui='.a:gui.' guifg='.a:guifg
  execute l:hightlight
endfunction


" This functions will conceal the begin_pattern and end_pattern, and hightlight the 
" text contained in the pattern with  termfg for terminal color and guifg for gui color. 
" In order to do this, it will create regions with names:
" the value of name_region
" the value of name_region concatenated with _begin
" the value of name_region concatenated with _end
function rkc#fun#RkcHiRegionConceal(name_region, begin_pattern, end_pattern, term, termfg, gui, guifg)
  " If we want to use string variables in a command, we can create a string that contains the command with
  " the value of variable, then execute the string.
  
  let l:name_region_begin = a:name_region.'_begin'
  let l:name_region_end = a:name_region.'_end'

  " not sure contained is necessary in these two strings (at the contrary of 'contains=' that is
  " necessary in l:syntax_region
  let l:syntax_begin ='syntax match '.l:name_region_begin.' "'.a:begin_pattern.'" contained conceal'
  let l:syntax_end='syntax match '.l:name_region_end.' "'.a:end_pattern.'" contained conceal'

  execute l:syntax_begin
  execute l:syntax_end

  " without keepend, the end doesnt seems to work : the hightlight continue after it.
  let l:syntax_region = 'syntax region '.a:name_region.' start="'.a:begin_pattern.'" end="'.a:end_pattern.'"  contains='. l:name_region_begin.','.l:name_region_end.'  keepend' 
  execute l:syntax_region
  let l:hightlight = 'highlight def '.a:name_region.' cterm='.a:term.' ctermfg='.a:termfg.' gui='.a:gui.' guifg='.a:guifg
  execute l:hightlight
endfunction


" This functions will color the pattern with termfg terminal color and guifg for gui color. 
" In order to do this, it will create a match with name the value of name_match
function rkc#fun#RkcHi(name_match, pattern, term, termfg, gui, guifg)
  " If we want to use string variables in a command, we can create a string that contains the command with
  " the value of variable, then execute the string.
  
  let l:syntax = 'syntax match '.a:name_match .' "'.a:pattern.'"'
  execute l:syntax
  let l:hightlight = 'highlight def '.a:name_match.' cterm='.a:term.' ctermfg='.a:termfg.' gui='.a:gui.' guifg='.a:guifg
  execute l:hightlight
endfunction
